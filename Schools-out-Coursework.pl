%Schools Out


subject(science).
subject(english).
subject(math).
subject(gym).
subject(history).


activity(antiquing).
activity(waterskiing).
activity(spelunking).
activity(sightseeing).
activity(camping).



destination(maine).
destination(virginia).
destination(oregon).
destination(california).
destination(florida).

teacher(gross).
teacher(knight).
teacher(mcEvoy).
teacher(appleton).
teacher(parnell).




solve:-
subject(GrossSubject),subject(McevoySubject), subject(AppletonSubject),  subject(ParnellSubject), subject(KnightSubject),
all_different([GrossSubject, McevoySubject, AppletonSubject, ParnellSubject, KnightSubject ]),




activity(GrossActivity),activity(McevoyActivity),activity(AppletonActivity),activity(ParnellActivity),activity(KnightActivity),
all_different([GrossActivity, McevoyActivity, ParnellActivity, KnightActivity, AppletonActivity ]),

destination(GrossDestination),destination(McevoyDestination),destination(AppletonDestination),destination(ParnellDestination),destination(KnightDestination),
all_different([GrossDestination, McevoyDestination, AppletonDestination, ParnellDestination, KnightDestination]),

Quads = [ [gross, GrossSubject, GrossDestination, GrossActivity ],
            [knight, KnightSubject, KnightDestination, KnightActivity ],
            [mcevoy, McevoySubject, McevoyDestination, McevoyActivity],
            [appleton, AppletonSubject,AppletonDestination, AppletonActivity], 
            [parnell, ParnellSubject,ParnellDestination, ParnellActivity] ],



%member([teacher, subject, destination, activity], Quads),


%the person who is going to main is not the gym teacher and not going sightseeing
\+ member([_, gym, maine, sightseeing], Quads),


%mrs gross is not the woman going camping
\+ member([gross, _, _, camping], Quads),

%mr knight is not going camping
\+ member([knight, _, _, camping], Quads),

%mr mcevoy is not going camping
\+ member([mcevoy, _, _, camping], Quads),

%mr knight is not going antiquing
\+ member([knight, _, _, antiquing], Quads),

%mr mcevoy is not going antiquing
\+ member([mcevoy, _, _, antiquing], Quads),
%if the woman going to virginia is the english teacher then she is appleton, else, she is parnell who is going spelunking
\+ member([appleton, english, virginia, _], Quads); member([parnell, _, virginia,spelunking], Quads),

%one woman is going antiguing
member([_, _,_,antiguing], Quads),

%mrs gross teaches either science or math. 
%if mr gross is going to florida then she will be antiquing, otherwise she %will be going to california

member([gross, maths,_,_], Quads); member([gross, science,_,_], Quads),
member([gross, _,florida,antiguing], Quads); member([gross, _,california,_], Quads),

%mr mcevoy, the history teacher is either going to maine or oregon
member([mcevoy, history, maine,_], Quads);member([mcevoy, history, oregon,_], Quads),

%the science teacher who is going waterskiing is either going to florida or %california
member([_,science, florida, waterskiing], Quads);member([_,science, california, waterskiing], Quads),



%if the woman going to virginia is the english teacher then she is appleton, else, she is parnell who is going spelunking
%\+ member([appleton, english, virginia, _], Quads); member([parnell, _, virginia,spelunking], Quads),




tell(gross, GrossSubject, GrossDestination, GrossActivity),
tell(parnell, ParnellSubject, ParnellDestination, ParnellActivity),
tell(mcevoy, McevoySubject, McevoyDestination, McevoyActivity),
tell(knight, KnightSubject, KnightDestination, KnightActivity),
tell(appleton, AppletonSubject, AppletonDestination, AppletontActivity).

% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.



all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

tell(X, Y,W, Z) :-
write('Teacher'), write(X), write(' teaches '), write(Y),
write(' went for holidays in'), write(W), write('and engaged in'), write(Z), write('holiday activity'), nl.














































